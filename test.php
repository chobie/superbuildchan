<?php
ini_set('display_errors',"On");
ini_set('log_errors',"On");
putenv("LANG=ja_JP");
setlocale(LC_ALL, 'ja_JP.UTF-8');
error_reporting(E_ALL ^ E_STRICT);

@date_default_timezone_set((@date_default_timezone_get() == "") ? "Asia/Tokyo" : @date_default_timezone_get());
mb_detect_order("ASCII,JIS,EUC,UTF-8,SJIS");
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");

require "SuperBuildChan/Autoloader.php";
SuperBuildChan_AutoLoader::register();

$loader = new SuperBuildChan_Loader_FileSystem();
$bot = new SuperBuildChan_Worker($loader);
$bot->loadConfig("buildchan.config.php");

$bot->listen();