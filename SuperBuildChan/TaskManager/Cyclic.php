<?php

class SuperBuildChan_TaskManager_Cyclic{
  public $tasks;
  public $now;

  public function execute(){
    $this->now = time();
    if(is_array($this->tasks)){
      foreach($this->tasks as $item):

        if($item->getNext() <= $this->now){
          $item->process();
        }
      endforeach;
    }
  }

  public function __construct(){
    $this->tasks = array();
  }
  
  public function add($name,$command,$interval){
    $task = new SuperBuildChan_Task_Test();
    $task->setName($name);
    $task->setCommand($command);
    $task->setInterval($interval);
    $this->tasks[] = $task;
  }
}