<?php

class SuperBuildChan_Autoloader
{
  public static function register()
  {
    spl_autoload_register(array("SuperBuildChan_Autoloader", 'loadClass'));
  }

  public static function loadClass($class)
  {
		$dir = getcwd();
    $class_name = str_replace('_', DIRECTORY_SEPARATOR, $class);
    if (0 === strpos($class, "SuperBuildChan"))
    {
      $file = $dir.DIRECTORY_SEPARATOR. $class_name .'.php';
      if (file_exists($file))
      {
        require $file;
	      return;
      }
    }
    return false;
  }
}
