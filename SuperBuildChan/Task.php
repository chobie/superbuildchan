<?php
abstract class SuperBuildChan_Task implements SuperBuildChan_TaskInterface{
  public $name;

  public function setName($name){
    $this->name = $name;
  }

  final public function process(){
    $this->execute();
    $this->update();
  }

}