<?php
class SuperBuildChan_Task_Test extends SuperBuildChan_Task{
  public $task;
  public $next_at;
  public $last_at;
  public $interval;
  public $command;


  public function getCommand(){
    return $this->command;
  }

  public function getInterval(){
    return $this->interval;
  }
  public function setInterval($interval){
    $this->interval = (int)$interval;
  }

  public function setCommand(SuperBuildChan_Command $command){
    $this->command = $command;
  }

  public function __construct(){
    $this->next_at = time() + $this->getInterval();
  }
  
  public function execute(){
    printf("[taskname:%s] invoked at %s.[memory useage:%sKb]\r\n",$this->name,date("Y-m-d H:i:s"),round(memory_get_usage()/1024,1));
    $this->getCommand()->execute();
  }

  public function update(){
    $this->next_at = strtotime("now")+$this->getInterval();
  }
  
  public function getNext(){
    return $this->next_at;
  }
}
