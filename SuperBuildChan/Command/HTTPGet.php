<?php
class SuperBuildChan_Command_HTTPGet extends SuperBuildChan_Command{
  public function execute(){
    $url = $this->command;
    $scheme = parse_url($url);

    $socket = @fsockopen($scheme['host'],80,$errno,$errstr,3);
		$result = "";
    if(is_resource($socket)){
    	$request  = "GET " . $scheme['path'] . " HTTP/1.0\r\n";
    	$request .= "Host: " . $scheme['host'] . "\r\n";
    	$request .= "\r\n";

			fwrite($socket,$request);
    	while(!feof($socket)){
    		$result .= fread($socket,8192);
    	}
    	fclose($socket);
    	
    	list($header,$body) = preg_split("/^\r?\n/m",$result,2);
			$headers = preg_split("/\r?\n/",trim($header));
			if(strpos($headers[0],"200") === false){
				var_dump("HTTP response does not 200.");
			}

			if(strpos($body,"Parse error")!== false){
				var_dump("Found Parse error");
			}

			if(strpos($body,"Fatal error")!== false){
				var_dump("Found Fatal error");
			}

    }else{
    	var_dump("can't connect");
    }
  }
}
