<?php
class SuperBuildChan_Command_PHPLint extends SuperBuildChan_Command{
	public $on_error;

	public function __construct($command,$on_error=null){
		$this->command = $command;
		if(!empty($on_error))
			$this->on_error = $on_error;
	}

  public function execute(){
    $dir = dirname($this->command);
    $dir_iterator = new RecursiveDirectoryIterator($dir);
    $iterator = new RecursiveIteratorIterator($dir_iterator,RecursiveIteratorIterator::SELF_FIRST);
    
    foreach($iterator as $it){
      $file = $it;

      if(is_file($file) && strrpos($file,".php") !== false){
        $command = "php -l {$file}";
        $descriptorspec = array(1=>array('pipe','w'),2=>array('pipe','w'));
        $pipes = array();
        $resource = proc_open($command,$descriptorspec,$pipes,null);
        if(is_resource($resource)){
          
        $stdout = stream_get_contents($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        foreach($pipes as $pipe) fclose($pipe);
        
        $status = trim(proc_close($resource));
        echo "Execute: {$command}" . PHP_EOL;
        echo "[Ouput]:\n"
            .$stdout . PHP_EOL;

        if($status<0){
         	var_dump($stdout);
        }

        }else{
          echo "Could not execute . ";
        }
      }
    }
  }
}