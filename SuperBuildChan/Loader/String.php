<?php

class SuperBuildChan_Loader_String extends SuperBuildChan_Loader{
  public $name;
  
  public function __construct($name=null){
    if(!empty($name)){
      $this->name = $name;
    }
  }

  public function getResource($name){
    $this->name = $name;
    return $name;
  }
  
  public function getName(){
    return $this->name;
  }
}
