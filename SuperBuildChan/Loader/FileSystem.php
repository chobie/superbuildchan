<?php
class SuperBuildChan_Loader_FileSystem extends SuperBuildChan_Loader{
  public $name;
  
  public function getResource($name){
    $this->name = $name;
    if(file_exists($name)){
      return file_get_contents($name);
    }
  }
  
  public function getName(){
    return $this->name;
  }
}