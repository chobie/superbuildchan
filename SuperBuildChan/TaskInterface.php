<?php
interface SuperBuildChan_TaskInterface{
  public function setName($name);
  public function setCommand(SuperBuildChan_Command $command);
  public function getCommand();
  public function setInterval($interval);
  public function getInterval();
  public function getNext();
  public function process();
  public function execute();
  public function update();
}