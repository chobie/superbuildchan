<?php
# SuperBuildChan Config File.
# 注意： cyclicイベントは可能な限りの範囲でn秒毎に実行されます。
# 該当のコードを実行している間はcyclicイベントが走らないことに注意しましょう。
# （厳密なインターバルを求めるべきアプリケーションではありません）
#
# $cyclic->add(<name>,<SuperBuildChan_Command>,<int interval>);

# 時報(動作確認をするときに役に立ちます。)
//$cyclic->add("test",new SuperBuildChan_Command_PHPCode('echo date("Y-m-d H:i:s") . PHP_EOL;'),1);

# n秒毎に該当ディレクトリ以下のPHPファイルをlint`php -l`します
//$cyclic->add("php lint",new SuperBuildChan_Command_PHPLint("./"),1800);

# 該当URLが200 OKを返すか調べます
//$cyclic->add("HTTP get check", new SuperBuildChan_Command_HTTPGet("http://yahoo.co.jp/"),10);

# Note:
# 循環参照を含むコードの場合はPHPのGCが働きません。
# 循環参照があるコードを利用する場合は明示的に`unset`を行いメモリを
# 常に確保できるように努めてください。